#A simple task management simulator by rilt
import multiprocessing
import os
import psutil

import time
from croniter import croniter
from datetime import datetime, timedelta

def doJob(task_name):
    f = open("output.txt", "a")
    cur = current_milli_time()
    # print('Task %s is running, current time = %s' % (task_name,cur))
    f.write('DoJob triggered in task %s, current time = %s\n' % (task_name,cur))
    f.close()

# Round time down to the top of the previous minute
def roundDownTime(dt=None, dateDelta=timedelta(minutes=1)):
    roundTo = dateDelta.total_seconds()
    if dt == None : dt = datetime.now()
    seconds = (dt - dt.min).seconds
    rounding = (seconds+roundTo/2) // roundTo * roundTo
    return dt + timedelta(0,rounding-seconds,-dt.microsecond)

# Get next run time from now, based on schedule specified by cron string
def getNextCronRunTime(schedule):
    return croniter(schedule, datetime.now()).get_next(datetime)

# Sleep till the top of the next minute
def sleepTillTopOfNextMinute():
    t = datetime.utcnow()
    sleeptime = 60 - (t.second + t.microsecond/1000000.0)
    time.sleep(sleeptime)

def current_milli_time():
    return round(time.time() * 1000)

def pauseTerminal():
    input("Press any key to continue...")

def checkInputVaild(name, schedule):
    if len(task_name) < 1 or len(task_name) > 6 or len(schedule) < 9:
        return False
    else:
        #TODO 
        #Futher check crontab string is valid
        return True
class Task(multiprocessing.Process):
    def __init__(self, name, schedule, queue):
        multiprocessing.Process.__init__(self)
        self.name = name
        self.schedule = schedule
        self.is_paused = False
        self.is_running = False
        self.queue = queue

    def run(self):
        # print("Running ", self.name)
        self.is_running = True
        # Put some data we want to share to another task
        # In this case we put a dict of task data 
        task_dict = {
            "name": self.name,
            "pid": self.pid
            # Something more
        }
        self.queue.put({self.name : task_dict})

        #Run doJob at scheduled times
        nextRunTime = getNextCronRunTime(self.schedule)
        while True:
            roundedDownTime = roundDownTime()
            if (roundedDownTime == nextRunTime):
                # It's time to doJob()
                doJob(self.name)
                nextRunTime = getNextCronRunTime(self.schedule)
            elif (roundedDownTime > nextRunTime):
                # We missed an execution. Error. Re initialize.
                nextRunTime = getNextCronRunTime(self.schedule)
            sleepTillTopOfNextMinute()

    # def getData(self):
    #     self.data

class TaskManager(object):
    def __init__(self, numProcs):
        self.num_process = numProcs
        self.tasks = []
        self.running_queue = multiprocessing.Queue()

    def initATask(self, task_name, schedule):
        # now = datetime.now()
        # current_time = now.strftime("%H:%M:%S")
        # print('\nTask %s start at %s' % (task_name, current_time))
        aTask = Task(str(task_name), schedule, self.running_queue)
        self.tasks.append(aTask)

    def run(self):
        #initize some tasks
        schedule = "* * * * *" # Run every minutes
        for i in range(self.num_process):
           self.initATask(i, schedule) 
        self.startAll()
        print("Tasks initialized")
        time.sleep(0.1)
    
    def addTask(self, task_name, schedule):
        if any( proc.name == str(task_name) for proc in self.tasks):
            print("Task name is not ok, confliction found")
        elif not checkInputVaild(task_name, schedule):
            print("Invalid task name or schedule")
        else:
            self.initATask(task_name, schedule)
            print("Task %s added with scron: %s" %(task_name, schedule))
        time.sleep(0.1)
        pauseTerminal()

    def removeTask(self, task_name):
        not_found = True
        for task in self.tasks:
            if task.name == task_name:
                not_found = False
                print('Task found, terminating ...')
                if task.is_running : task.terminate()
                self.tasks.remove(task)
                print('Task %s removed' % (task_name))
        if not_found:
            print('Task %s not found, please check the name' % (task_name))
        pauseTerminal()

    def startAll(self):
        for idx, task in enumerate(self.tasks):
            task.start()
            task.is_running = True
            task.data = self.running_queue.get()
            #Update task data
            self.tasks[idx]=task

    def start(self, task_name):
        not_found = True
        for idx, task in enumerate(self.tasks):
            if task.name == task_name:
                not_found = False
                if not task.is_running:
                    task.start()
                    task.is_running = True
                    self.tasks[idx]=task
                    print ("Task %s started ..." % task_name)
                else:
                    print ("Task %s is running ..." % task_name)
        if not_found:
            print('Task %s not found, please check the name' % (task_name))
        pauseTerminal()

    def stop(self, task_name):
        not_found = True
        for idx, task in enumerate(self.tasks):
            if task.name == task_name:
                not_found = False
                if task.is_running:
                    task.terminate()
                    print ("Task %s stopped ..." % task_name)
                     # We cannot start a process twice, so this task need to be recreated
                    self.tasks.remove(task)
                    self.tasks.append(Task(task.name, task.schedule, self.running_queue))
                else:
                    print ("Task %s is stopped ..." % task_name)
        if not_found:
            print('Task %s not found, please check the name' % (task_name))
        pauseTerminal()

    def pause(self, task_name, cmd):
        not_found = True
        for task in self.tasks:
            if task.name == task_name:
                not_found = False
                if task.is_running:                    
                    p = psutil.Process(task.data[task.name]["pid"])
                    if cmd == 'pause':   
                        p.suspend()
                        task.is_paused = True
                        print ("Task %s paused ..." % task_name)
                    elif cmd == 'resume':
                        p.resume()
                        task.is_paused = False
                        print ("Task %s resumed ..." % task_name)
                else:
                    print ("Task %s is not running ..." % task_name)
        if not_found:
            print('Task %s not found, please check the name' % (task_name))
        pauseTerminal()

    def importTask(self, list_task):
        for task_str in list_task:
            task = task_str.split(",")
            if any(proc.name == str(task[0]) for proc in self.tasks):
                print("Task name is not ok, confliction found, skipped")
            elif not checkInputVaild(task[0], task[1]):
                print("Invalid task name or schedule, skipped")
            else:
                self.initATask(task[0], task[1])
                print("Task added: ", task)
        pauseTerminal()
    def exportTask(self):
        with open("exported.txt", "w") as file:
            for task in self.tasks:
                file.write("%s,%s\n" %(task.name,task.schedule))
            file.close()
        print("Tasks exported to ", os.getcwd() + "/exported.txt")
        pauseTerminal()

    def info(self):
        print('------------------------')
        print("Task name  -   Status")
        for task in self.tasks:
            if task.is_running:
                status = 'running'
                if task.is_paused:
                    status = 'paused'
            else:
                status = 'stopped'
            name = task.name.ljust(8)
            print(" %s  -   %s" %(name, status))
        print('------------------------')
    def get_queue(self):
        # return list(self.running_queue.queue)
        # return [self.running_queue.get() for _ in range(self.running_queue.qsize())]
        return self.tasks

    def killAll(self):
        # for task in self.tasks:
        #     if task.is_running:
        for task in self.tasks:
            try:
                task.terminate()
            except:
                pass 
        os._exit(0)

if __name__ == '__main__':   
    aTaskManger = TaskManager(5)
    aTaskManger.run()
    print (aTaskManger.get_queue()) 

    pauseTerminal()
    while True:
        os.system('cls' if os.name == 'nt' else 'clear')
        aTaskManger.info()
        choosen = input("1. Add task \n2. Remove Task \n3. Start task \n4. Stop task \n5. Import task \n6. Export task\n7. Pause task \n8. Resume task \n9.Exit: \nYours Choice: ")
        if choosen.isnumeric():
            if choosen == '9' : aTaskManger.killAll()
            if choosen == '1' :
                task_name = input("Set task name:  ")
                schedule = input("Cron string: ")
                aTaskManger.addTask(task_name, schedule)
            if choosen == '2' :
                task_name = input("Task name to remove:  ")
                aTaskManger.removeTask(task_name)
                print()
            if choosen == '3' :
                task_name = input("Task name to start:  ")
                aTaskManger.start(task_name)
                print()
            if choosen == '4' :
                task_name = input("Task name to stop:  ")
                aTaskManger.stop(task_name)
                print()
            if choosen == '5' :
                # Import list of tasks from file
                list_name = os.getcwd() + "/" + input("filename :  ")
                if os.path.exists(list_name):
                    with open(list_name) as file:
                        list_task = [line.strip() for line in file]
                        file.close()
                    aTaskManger.importTask(list_task)
                else:
                    print("File does not exist.")
                    pauseTerminal()
                print()
            if choosen == '6' :
                # Export list of tasks to file
                aTaskManger.exportTask()
            if choosen == '7' :
                task_name = input("Task name to pause:  ")
                aTaskManger.pause(task_name, 'pause')
            if choosen == '8' :
                task_name = input("Task name to resume:  ")
                aTaskManger.pause(task_name, 'resume')