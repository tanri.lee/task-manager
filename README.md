# A simple task management simulator by rilt
### Required module
    croniter
    psutil
Tested on Python 3.9

### Run the simulator
    pip install -r requirements.txt
    python app.py


### Explanation
Suppose task as processs in Python, We have somthing to do with it:
1. Add new task - Initilize new process : Create new process with unique name and a schedule time to do a Job in crontime format. See [Crontab Guru](https://crontab.guru/),
then add this new task to Task Manager's task list. 
I use a moudle called  [Croniter](https://github.com/kiorky/croniter) and some calculation to schedue a specific time for the task to do a Job - which will write a text into output.txt. 
2. Remove task: remove a task from task list. If task is running, we need to stop it first (terminate proccess before move it out from process list).

3. Start task: simply call process.start() and update some data. Because every python process has it's own separate memory space, I use a Queue to share data among them. If Task A want to share something, it will put data into multiprocessing queue, then process B will get it from that queue if it want to. The demonstrates of shared memory is not completed yet, I just use the dat(process id) to pause/resume  a task.

4. Stop task: terminate a process, cause we can not start a process twice, we need to recreate a stopped task, keep shared data and put it back to the task list to start it again.
5. Pause task: I use [psutil](https://github.com/giampaolo/psutil) moudle to pause/resume a task.Just call it from task manager with task name (process id) from shared data.

6. Import task: Import a list of tasks in a text file. 
    syntax of import and export files are the same:
```
    list_task.txt
    task1,* 1 * * *
    task2,*/5 * * * *
    task3,* * 2 * *
```

7. Export task: Export all task  to a text file.

### Demo
Some demo on youtube
[![](http://img.youtube.com/vi/ihO-Qm_g47s/0.jpg)](http://www.youtube.com/watch?v=ihO-Qm_g47s "Task Manager Demo")
